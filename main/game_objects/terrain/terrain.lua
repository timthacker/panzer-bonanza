-- Put functions in this file to use them in several other scripts.
-- To get access to the functions, you need to put:
-- require "my_directory.my_file"
-- in any script using the functions.

local M = {}

local function getStreamIndex(x, y)
	return (y * M.width * 4) + (x * 4) + 1
end


local function round(value)
	return value % 1 >= 0.5 and math.ceil(value) or math.floor(value)
end

function M.draw_terrain_point(x, y, color)
	local index = getStreamIndex(x, y)
	M.stream[index + 0] = color.r
	M.stream[index + 1] = color.g
	M.stream[index + 2] = color.b
	M.stream[index + 3] = color.a
	M.change = true
end

function M.add_ground(x, y)
	if (x < 0 or y < 0 or x > M.width or y > M.height) then
		return
	end
	M.draw_terrain_point(x, y, {
		r = 0,
		g = math.random(200, 255),
		b = 0,
		a = 255
	})
end

function M.remove_ground(x, y)
	if (x < 0 or y < 0 or x > M.width or y > M.height) then
		return
	end
	M.draw_terrain_point(x, y, {
		r = 0,
		g = 0,
		b = 0,
		a = 0
	})
end

function M.explosion(x, y, radius)
	local xr = round(x)
	local yr = round(y)
	for w = -radius, radius do
		for h = -radius, radius do
			if (w * w + h * h <= radius * radius) then
				M.remove_ground(xr + w, yr + h)
			end
		end
	end
end

function M.collides(x, y)
	if (x < 0 or y < 0 or x > M.width or y > M.height) then
		return true -- treats out of bounds as solid
	end
	local index = getStreamIndex(round(x), round(y))
	return M.stream[index + 3] > 0 -- checks alpha is above 0
end

local accuracy = 5
function M.getNormal(x, y)
	local avg = vmath.vector3(0, 0, 0)
	for w = -accuracy,accuracy do
		for h = -accuracy,accuracy do
			if (M.collides(x + w, y + h)) then
				avg = avg - vmath.vector3(w, h, 0)
			end
		end
	end
	local length = vmath.length(avg)
	if (length == 0) then
		return vmath.vector3(0, 1, 0)
	else
		return vmath.normalize(avg)
	end
end

local function polynomial2(a, b, c, x)
	return a * math.pow(x, 2) + b * x + c
end

local function polynomial3(x)
	return -0.000003125 * math.pow(x, 3) + 0.00375 * math.pow(x, 2) - 1.125 * x + 300
end


local function generateCoefficients(x1, y1, y0)
	local c = y0
	local a = -((y1 - y0) / math.pow(x1, 2))
	local b = -2 * x1 * a
	return { a = a, b = b, c = c }
end

local function generateHeightMap(algorithm, maxX, maxH)
	local hm = {}
	if (algorithm == "mound") then
		local coeff = generateCoefficients(506, math.random(250, 400), math.random(50, 240))
		for x =1, maxX do
			hm[x] = polynomial2(coeff.a, coeff.b, coeff.c, x)
		end
	elseif (algorithm == "bowl") then
		local coeff = generateCoefficients(506, math.random(50, 150), math.random(350, 500))
		for x =0, maxX do
			hm[x] = polynomial2(coeff.a, coeff.b, coeff.c, x)
		end
	elseif (algorithm == "low-bowl") then
		local coeff = generateCoefficients(500, 25, 75)
		for x =0, maxX do
			hm[x] = polynomial2(coeff.a, coeff.b, coeff.c, x)
		end
	elseif (algorithm == "sway") then
		for x =0, maxX do
			hm[x] = polynomial3(x)
		end
	elseif (algorithm == "break-through") then
		for x=0, maxX do
			if (x > 450 and x < 550) then
				hm[x] = maxH
			else
				hm[x] = 100
			end
		end		
	elseif (algorithm == "trig-fractal") then
		local iterations = 8
		local F = {}
		local A = {}
		local O = {}
		local height = math.random(200, 201)
		for i=1,iterations do
			F[i] = math.random() * 0.04 -- FREQUENCY :: affects bumpiness
			A[i] = math.random() * 60 - 30 -- APMPLITUDE :: affects height of slopes
			O[i] = math.random() * 6.2 -- OFFSET :: simply for variance
		end
		for x =0, maxX do
			hm[x] = 0
			for i=1,iterations do
				hm[x] = hm[x] + A[i] * math.sin(F[i] * (x + O[i]))
			end
			hm[x] = hm[x] + height
		end
	elseif (algorithm == "flat") then
		for x =0, maxX do
			hm[x] = 10
		end
	end
	return hm
end

local function interpolate(low, high, perc)
	local value = high * perc + low* (1 - perc)
	if (value > 255) then
		return 255
	elseif (value < 0) then
		return 0
	else
		return value
	end
end

function M.initialiseTerrain(_width, _height, seed, color)
	M.height = _height
	M.width = _width
	M.buffer = buffer.create(M.width * M.height, { {name=hash("rgba"), type=buffer.VALUE_TYPE_UINT8, count=4 } } )
	M.stream = buffer.get_stream(M.buffer, hash("rgba"))
	local lowC = { r = 0, g = 200, b = 0 }
	local highC = { r = 0, g = 50, b = 0 }
	local variance = 0.4
	
	local height_map = generateHeightMap("trig-fractal", _width - 1, _height - 1)
	
	for x = 0, M.width - 1 do
		local h = round(height_map[x])
		for y = 0, h do
			M.draw_terrain_point(x, y, {
				r = color.r,
				g = color.g,
				b = color.b,
				a = 255
			})
		end
	end
	M.change = true
end



function M.getBuffer()
	return M.buffer
end

function M.getStream()
	return M.stream
end

function M.hasTerrainChanged()
	return M.change
end

function M.setChange(value)
	M.change = value
end

return M